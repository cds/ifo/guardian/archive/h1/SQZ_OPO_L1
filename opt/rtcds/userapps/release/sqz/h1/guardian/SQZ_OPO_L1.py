# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_OPO.py $
# Nutsinee Kijbunchoo Jan 29, 2019
# This guardian will lock the OPO using the original scheme

import sys
import time
from guardian import GuardState, GuardStateDecorator, NodeManager
import cdsutils as cdu

nominal = 'LOCKED_CLF_DUAL'

#############################################
#Functions

def OPO_locked():
    opo_trans_norm = 0.5
    return ezca['SQZ-OPO_TRANS_DC_NORMALIZED'] > opo_trans_norm

def TTFSS_LOCKED():
    flag = False
    if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
        flag = True
    return flag

def in_softfault():
    if ezca['SQZ-OPO_PZT_1_RANGE'] != 0:
        notify('PZT voltage limits exceeded.')
        log('PZT voltage limits exceeded.')
        return True
    elif not OPO_locked():
        notify('OPO not really locked or maybe locked in a wrong mode')
        log('OPO not really locked or maybe locked in a wrong mode')
        return True


def in_hardfault():
    if ezca['SQZ-LASER_IR_DC_ERROR_FLAG'] != 0:
        notify('Squeezer laser PD error')
        log('Squeezer laser PD error')
        return True

def EOM_OK():
    return abs(ezca['SQZ-FIBR_SERVO_EOMRMSMON']) < 4 


#############################################
#Decorator


class softfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_softfault():
            return 'DOWN'

class hardfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_hardfault():
            return 'IDLE'

class EOM_checker(GuardStateDecorator):
    def pre_exec(self):
        if not EOM_OK():
            ezca['SQZ-FIBR_SERVO_EOMEN']=0
            return 'CHECK_EOM'
            

#class Power2high(GuardStateDecorator):
#    def pre_exc(self):
#        if Power2high:
#           ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 0
#           notify('Pump input power > 23mW. Flipper down.')
#           return 'IDLE'
        
#############################################        
#nodes

#nodes = NodeManager(['SQZ_PLL','SQZ_CLF'])


#############################################        
#States

class INIT(GuardState):
    index = 0

    def main(self):
        return True

#        if OPO_locked():
#           return 'LOCKED'
#        else:
#           return 'DOWN'

class DOWN(GuardState):
    index = 1
    goto = True

    
    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 0

        #turn intensity loop servo off and clear history
        ezca.switch('SQZ-SHG_PWR_SERVO', 'INPUT' , 'OFF')
        ezca['SQZ-SHG_PWR_SERVO_RSET']=2

        ezca['SQZ-OPO_SERVO_IN1EN'] = 0
        ezca['SQZ-OPO_SERVO_IN2EN'] = 0 #never need this
        ezca['SQZ-OPO_SERVO_IN1POL'] = 1 #make sure polarity is correct
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0

        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] = 0
        ezca['SQZ-OPO_PZT_1_OFFSET'] = 1 #just to leave room away from negative

        
    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False
    #goto = True

    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
            notify('Scan timeout. Check trigger level.')
            log('Scan timeout')   
         
        notify('Cannot lock OPO. Check pump light on SQZT6.')
        log('Cannot lock OPO. Check pump light on SQZT6')
        return (ezca['SQZ-SHG_GR_DC_NORMALIZED'] > 0.3) and (ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] < 30) #20mW to protect to fiber
   

####################################################
# CLF LOCKING
####################################################
class PREP_LOCK_CLF(GuardState):
    index = 14
    request = False
    
    def main(self):
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1 
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 1
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 1
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 0
        notify('CLF light in.')

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 34 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 33:
            ezca['SQZ-OPO_TEC_SETTEMP'] = 33.33

    def run(self):
        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 34 and ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 33:
            return True



class SCANNING_CLF_DUAL(GuardState):
    index = 15
    request = False


    def main(self):
        #turn off intensity servo and clear history before scanning

        ezca.switch('SQZ-SHG_PWR_SERVO', 'INPUT' , 'OFF')
        ezca['SQZ-SHG_PWR_SERVO_RSET']=2 
       
        if ezca['SYS-MOTION_C_SHUTTER_H_STATE'] == 1:
           notify('CLF shutter closed. Check fiber switch before open. CLF should go in.')
           log('CLF shutter closed. Check fiber switch before open. CLF should go in.') 
           return 'SCANNING_CLF_DUAL'
            #Just send a message and continue. User should check whether if seed or CLF is being used (NK Aug28).

        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1

        #make sure loop is open and boosts are off
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD']=100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT']=2 # Set PZT scan trigger to 'And'. 
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 0 # This looks at refl trans normalized AND 6MHz RF mon, 1 for lock seed dual
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.6
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = 0.5
      
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT


    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False



def LOCKING():
    class LOCKING(GuardState):
        request = False

        def main(self):
            ezca['SQZ-OPO_SERVO_IN1EN'] = 1 #engage OPO locking
            time.sleep(1)

        def run(self):
            #if ezca['SQZ-OPO_PZT_1_RANGE'] != 0: #if not okay
            #    notify('PZT out of range. Try again.')
            #    log('PZT out of range. Try again.')
            #    return 'IDLE'

            #check if OPO is locked fine. Then engage boosts
            if OPO_locked():
                ezca['SQZ-OPO_SERVO_COMBOOST']= 1
                ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 1
                time.sleep(0.5)

                return True
            else:
                return 'IDLE'

    return LOCKING



LOCKING_CLF_DUAL = LOCKING()
LOCKING_CLF_DUAL.index = 16



class LOCKED_CLF_DUAL(GuardState):
    index = 17
    
    #@softfault_checker
    #@hardfault_checker
    
    def main(self):     
        if OPO_locked():
            log('OPO locked')
            notify('OPO locked')

            if abs(ezca['SQZ-SHG_PWR_REFL_OFFSET']+ezca['SQZ-OPO_REFL_DC_POWER']) < 0.5:
                ezca['SQZ-SHG_PWR_SERVO_RSET']=2
                time.sleep(3)
                ezca.switch('SQZ-SHG_PWR_SERVO', 'INPUT' , 'ON')
                time.sleep(3)
            else:
                log('Check Intensity Servo Offset, not turning on servo') #just warn user and move on



    #@softfault_checker
    #@hardfault_checker
    #@EOM_checker
    def run(self):
        OPOREFLave = cdu.avg(1,'H1:SQZ-OPO_REFL_DC_POWER', stddev = True)
        if OPOREFLave[1] > 0.005:
            log('OPO REFL NOISY')
        if not TTFSS_LOCKED():
            ezca.switch('SQZ-SHG_PWR_SERVO', 'INPUT' , 'OFF')
            ezca['SQZ-SHG_PWR_SERVO_RSET']=2

        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            log('OPO output rails')    
            return 'SCANNING_CLF_DUAL'

        return True



class CHECK_EOM(GuardState):
    index = 18
    goto = False #So that Guardian won't find the shortest path to LOCKED with this state
    request = False

    @softfault_checker #this will make OPO rescan if it's not locked
    def run(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1 #close beam diverter
        notify('EOM noisy or railed, disengaged.')
        log('EOM noisy or railed, disengaged.')

        if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
            ezca['SQZ-FIBR_SERVO_EOMEN'] = 1

        if (ezca['SQZ-FIBR_SERVO_EOMEN'] == 1) and EOM_OK(): #If manually engage EOM and it doesn't rail
            return 'LOCKED_CLF_DUAL'

        #if OPO_locked() and (ezca['PSL-FSS_TPD_DC_OUTPUT']>1.5): #If OPO locks and ref cav OK
        #    return 'LOCKED_ON_DUAL'
        #if ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] >20:
        #    return 'IDLE'




    
####################################################
# SEED HIGH POWER LOCKING - For IFO Alignment
####################################################
class PREP_LOCK_SEED_HIGH(GuardState):
    index = 24
    request = False
    
    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 0
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1

        ezca['SQZ-HD_FLIPPER_CONTROL'] = 0

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 50:
            ezca['SQZ-OPO_TEC_SETTEMP'] = 51.095

    def run(self):
        # Set SEED Power to ~>40mW    
        # changed to 60 for Dan's TCS work (Dec 6, 2018 NK)    
        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] < 57:
            self.counter = 0
            if self.counter == 0:
                ezca['SYS-MOTION_C_PICO_I_SELECTEDMOTOR'] = 6
                ezca['SYS-MOTION_C_PICO_I_CURRENT_STEPSIZE'] = 100
                ezca['SYS-MOTION_C_PICO_I_CURRENT_DRIVEDELAY'] = 4 
                self.counter += 1
            if ezca['SYS-MOTION_C_PICO_I_INUSE'] == 0:
                ezca['SYS-MOTION_C_PICO_I_MOTOR_6_DRIVE'] = 4 # DOWN =3 and UP = 4

        else:
            if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 50 and ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 53:
                notify('High power SEED light in.')
                return True



class SCANNING_SEED_HIGH(GuardState):
    index = 25
    request = False


    def main(self):

        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1

        #make sure loop is open and boosts are off
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0


        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD'] = 100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT'] = 2 #Set PZT scan trigger to 'Ch1 & Ch2'. 
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 1 # This looks at refl trans and OPO IR.
        
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.5
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = 5 # SQZ-OPO_IR_DC_POWER channel
        
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT


    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False




LOCKING_SEED_HIGH = LOCKING()
LOCKING_SEED_HIGH.index = 26



class LOCKED_SEED_HIGH(GuardState):
    index = 27

    @softfault_checker
    @hardfault_checker
    
    def run(self):
        if ezca['SQZ-OPO_IR_RESONANCE_SEED_NORM'] > 0.8:
            notify('OPO locked with high SEED.')
            return True
        else:
            notify('SEED_NORM < 0.8. Check dual resonance condition.')
        


    
####################################################
# SEED LOW POWER LOCKING - For NLG Measurement
####################################################
class PREP_LOCK_SEED_LOW(GuardState):
    index = 34
    request = False
    
    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 0
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1

        ezca['SQZ-HD_FLIPPER_CONTROL'] = 0

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 34 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 33:
            ezca['SQZ-OPO_TEC_SETTEMP'] = 33.36

    def run(self):
        # Set SEED Power to ~<1mW       
        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 1 or ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] < 0.5:
            self.counter = 0
            if self.counter == 0:
                ezca['SYS-MOTION_C_PICO_I_SELECTEDMOTOR'] = 6
                ezca['SYS-MOTION_C_PICO_I_CURRENT_STEPSIZE'] = 100
                ezca['SYS-MOTION_C_PICO_I_CURRENT_DRIVEDELAY'] = 4 
                self.counter += 1
            if ezca['SYS-MOTION_C_PICO_I_INUSE'] == 0:
                ezca['SYS-MOTION_C_PICO_I_MOTOR_6_DRIVE'] = 3 # DOWN =3 and UP = 4
            
        else:
            if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 33 and ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 34:
                notify('Low power SEED light in.')
                return True
                
            

class SCANNING_SEED_LOW(GuardState):
    index = 35
    request = False


    def main(self):

        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1

        #make sure loop is open and boosts are off
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0


        #ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD'] = 100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT'] = 2 # Set PZT scan trigger to 'Ch1 & Ch2'. 
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 1 # This looks at refl trans and OPO IR.

        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.5
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = 0.05 # SQZ-OPO_IR_DC_POWER channel
        
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT


    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False




LOCKING_SEED_LOW = LOCKING()
LOCKING_SEED_LOW.index = 36



class LOCKED_SEED_LOW(GuardState):
    index = 37

    @softfault_checker
    @hardfault_checker
    
    def main(self):
        if ezca['SQZ-OPO_IR_RESONANCE_SEED_NORM'] > 0.1:
            notify('OPO locked with low SEED, please measure NLG.')
            return True




            


################################################################
  
LOCKING_ON_ANY = LOCKING()
LOCKING_ON_ANY.index = 6
    
class SCANNING(GuardState):
    index = 4
    request = False
    
    def main(self):
        ezca['SQZ-OPO_PZT_1_OFFSET'] = 0
        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-FIBR_SERVO_RAMPEN']=1 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-FIBR_SERVO_EOMEN']=0 #turn off EOM
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD']=100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT']=0 #trigger on green trans (NK Aug 28)
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.8

    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False





class LOCKED(GuardState):
    index = 10

    @softfault_checker
    @hardfault_checker
    def main(self):
        if OPO_locked():
            if abs(ezca['SQZ-SHG_PWR_REFL_OFFSET']+ezca['SQZ-OPO_REFL_DC_POWER']) < 0.5:
                ezca['SQZ-SHG_PWR_SERVO_RSET']=2
                time.sleep(3)
                ezca.switch('SQZ-SHG_PWR_SERVO', 'INPUT' , 'ON')
                time.sleep(3)
            else:
                log('Check Intensity Servo Offset')


    @softfault_checker
    @hardfault_checker

    def run(self):
        OPOREFLave = cdu.avg(1,'H1:SQZ-OPO_REFL_DC_POWER', stddev = True)
        if OPOREFLave[1] > 0.005:
            log('OPO REFL NOISY')

        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            log('OPO output rails')    
            return 'SCANNING_CLF_DUAL'
        else:
            return True

        #if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
        #    if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
        #        notify('Scan timeout. Check trigger level.')
        #        log('Scan timeout')
        #        return 'IDLE'
        #    return True

        #if OPO_locked():
        #    return True
        #else:
        #    return 'IDLE' 





##################################################

edges = [
    ('INIT', 'DOWN'),
    #('DOWN', 'IDLE'),
    
    # LOCK CLF
    ('DOWN', 'PREP_LOCK_CLF'),
    ('PREP_LOCK_CLF', 'SCANNING_CLF_DUAL'),
    ('SCANNING_CLF_DUAL', 'LOCKING_CLF_DUAL'),
    ('LOCKING_CLF_DUAL', 'LOCKED_CLF_DUAL'),
    ('IDLE', 'SCANNING_CLF_DUAL'),
    ('CHECK_EOM', 'LOCKED_CLF_DUAL'),
    
    # LOCK SEED HIGH
    ('DOWN', 'PREP_LOCK_SEED_HIGH'),
    ('PREP_LOCK_SEED_HIGH', 'SCANNING_SEED_HIGH'),
    ('SCANNING_SEED_HIGH', 'LOCKING_SEED_HIGH'),
    ('LOCKING_SEED_HIGH', 'LOCKED_SEED_HIGH'),
    ('IDLE', 'SCANNING_SEED_HIGH'),

    
    # LOCK SEED LOW
    ('DOWN', 'PREP_LOCK_SEED_LOW'),
    ('PREP_LOCK_SEED_LOW', 'SCANNING_SEED_LOW'),
    ('SCANNING_SEED_LOW', 'LOCKING_SEED_LOW'),
    ('LOCKING_SEED_LOW', 'LOCKED_SEED_LOW') ,
    ('IDLE', 'SCANNING_SEED_LOW'),
    


    # LOCK ANY
    ('DOWN', 'SCANNING'),
    ('LOCKED','DOWN'),
    ('IDLE', 'SCANNING'),
    ('SCANNING', 'LOCKING_ON_ANY'),
    ('LOCKING_ON_ANY', 'LOCKED')]
    
